import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-4f251.appspot.com/o/';
  public images:string[] = [];

  constructor() {
    this.images[0] = this.path + 'biz.jpeg' + '?alt=media&token=c9780cfd-a652-417a-b768-d09df530127d';
    this.images[1] = this.path + 'entermnt.jpeg' + '?alt=media&token=2603a79e-5016-4048-8596-218a0268e940';
    this.images[2] = this.path + 'politics-icon.jpeg' + '?alt=media&token=6a2da539-918a-41b7-a8cb-5c5059fd6dba';
    this.images[3] = this.path + 'sport.jpeg' + '?alt=media&token=9e669e33-5b11-4e64-9f96-169f728694e3';
    this.images[4] = this.path + 'tech.jpeg' + '?alt=media&token=0156bac0-a8bb-4434-a9b2-6d10bdd63bd7';
   }
}
